<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulaire</title>
    </head>
    <body>
        <h1>Bonjour, qui êtes vous ?</h1>
        <div>
            <form action="/ma-page-de-traitement" method="post">
                <div>
                    <label for="name">Nom :</label>
                    <input type="text" id="name" name="name">
                </div>
                <div class="button">
                    <button type="submit">Ok !</button>
                </div>
            </form>
        </div>
    </body>
</html>
