<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.time.LocalDateTime" import="java.time.format.DateTimeFormatter" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bonjour</title></head><body>
        <h1>Bonjour !</h1>
        <p>Aujourd'hui nous sommes le <%=LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd MMM yyyy"))%>.</p>
        <p>Il est : <%= LocalDateTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss a"))%> .</p>
    </body>
</html>